<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="./css/style.css"/>

  <title>Over</title>
</head>

<body>

  <!-- De Nav-Bar -->
  <div class="row">
    <div class="col-12">
      <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="index.php">
          <img src="./img/mboutrecht.jpg" alt="logo" class="mbologo">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
          aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="about.php">Over <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="game.php">Game</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="inschrijven.php">Inschrijven</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                Opties
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="inlog.php">Inloggen</a>
              </div>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  </div>


  <!-- De Jumbotron -->
    <div class="row">
      <div class="col-12">
        <div class="jumbotron jumbotron-fluid">
          <div class="container">

            <h1 class="display-4">MBO Utrecht - Over</h1>
            <p>

            </p>
          </div>
        </div>
      </div>
    </div>

  <!-- Opening van de Main-Container -->
  <main class="container">

    <!-- De Content -->
    <div class="row">
      <div class="col-4">
        <h4 class="about"> 
          Neem een kijkje op onze game pagina! Je kan hier een leuk freerun spelletje spelen! Spring doe je met Z!
        </h4>
      </div>
      <div class="col-4">
        <img class="aboutimg" src="./img/keurmerk.jpg" alt="Img" >
      </div>
      <div class="col-4">
        <h4 class="about"> 
          Als het je bevalt kan je je inschrijven op onze inschrijfpagina! Vul je gegevens in en wie weet! 
        </h4>
      </div>
    </div>

    <div class="row">
      <div class="col-4">
        <img class="aboutimg" src="./img/game.png" alt="Img2" >
      </div>
      <div class="col-4">
        <h4 class="about">
          De opleiding is één van de best beoordeelde van ons land. Leerlingen hebben het hier erg naar hun zin! De opleiding duurt 3 jaar!
        </h4>
      </div>
      <div class="col-4">
        <img class="aboutimg" src="./img/inschrijf..png" alt="Img3" >
      </div>
    </div>

  </main>

      <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
      <script src=./js/app.js> </script> </body> </html> <style>
      <?php include './css/style.css'; ?>
    </style>

