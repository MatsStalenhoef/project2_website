<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    crossorigin="anonymous">
  <link rel="stylesheet" href="./css/style.css">

  <title>Inschrijven</title>
</head>

<body>

  <!-- De Nav-Bar -->
  <div class="row">
    <div class="col-12">
      <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="index.php">
          <img src="./img/mboutrecht.jpg" alt="logo" class="mbologo">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
          aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="about.php">Over</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="game.php">Game</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" href="inschrijven.php">Inschrijven <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                Opties
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="inlog.php">Inloggen</a>
              </div>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  </div>


  <!-- De Jumbotron -->
  <div class="row">
    <div class="col-12">
      <div class="jumbotron jumbotron-fluid">
        <div class="container">

          <h1 class="display-4">MBO Utrecht - Inschrijven</h1>
          <p>

          </p>
        </div>
      </div>
    </div>
  </div>

  <!-- Opening van de Main-Container -->
  <main class="container">

    <!-- Het inschrijf formulier -->
    <div class="row">
      <div class="col-6">
        <form action="./intake.php" method="post">
          <div class="form-group">
            <label for="firstname">Voornaam</label><label class="required">*</label>
            <input type="text" class="form-control" id="firstname" aria-describedby="firstnameHelp" placeholder="Invoer voornaam"
              name="firstname" required>
          </div>
          <div class="form-group">
            <label for="infix">Tussenvoegsel</label>
            <input type="text" class="form-control" id="infix" aria-describedby="infixHelp" placeholder="Invoer tussenvoegsel"
              name="infix">
          </div>
          <div class="form-group">
            <label for="lastname">Achternaam</label><label class="required">*</label>
            <input type="text" class="form-control" id="lastname" aria-describedby="lastnameHelp" placeholder="Invoer achternaam"
              name="lastname" required>
            
          </div>
          <div class="form-group">
            <label for="email">Email</label><label class="required">*</label>
            <input type="text" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Invoer email"
              name="email" required>
          </div>
          <div class="form-group">
            <label for="geboortedatum">Geboortedatum</label><label class="required">*</label>
            <input type="date" class="form-control" id="geboortedatum" aria-describedby="geboortedatumHelp" placeholder="Invoer geboortedatum"
              name="geboortedatum" required >
          </div>
          <div class="form-group">
            <label for="woonplaats">Woonplaats</label><label class="required">*</label>
            <input type="text" class="form-control" id="woonplaats" aria-describedby="woonplaatsHelp" placeholder="Invoer woonplaats"
              name="woonplaats" required>
          </div>
          <div class="form-group">
            <label for="vooropleiding">Vooropleiding</label><label class="required">*</label>
            <input type="text" class="form-control" id="vooropleiding" aria-describedby="vooropleidingHelp" placeholder="Invoer vooropleiding"
              name="vooropleiding" required>
            <small id="emailHelp" class="form-text text-muted">Wij zullen uw gegevens nooit delen met derden</small>
            <small id="emailHelp" class="form-text text-muted">*Deze velden zijn verplicht</small>
          </div>
          <button type="submit" class="btn btn-primary">Inschrijven</button>
        </form>
      </div>
      <div class="col-6">
        <h3 class="h3-requirements">
          De eisen voor de opleiding!
        </h3>
        <ul>
          <li class="li-requirements">Minimaal 15 jaar!</li>
          <li class="li-requirements">Vmbo-B of hoger vereist!</li>
        </ul>
      </div>
    </div>

  </main>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
    crossorigin="anonymous"></script>
  <script src=./js/app.js> </script> </body> </html> <style>
      <?php include './css/style.css'; ?>
    </style>