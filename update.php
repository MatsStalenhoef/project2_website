<?php

    include("./connect_db.php");
    
     //Zet het id uit de $_GET array in een gewone variabele
     $id = $_GET["id"];

     // Maak een select query
     $sql = "SELECT * FROM `student` WHERE `id` = $id";

     // Vuur de query af op de database
     $result = mysqli_query($conn, $sql);

     // Zet $result om in een leesbaar array
     $record = mysqli_fetch_assoc($result);
?>


<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    crossorigin="anonymous">
  <link rel="stylesheet" href="./css/style.css">

  <title>Update</title>
</head>

<body>

  <!-- De Nav-Bar -->
  <div class="row">
    <div class="col-12">
      <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="index.php">
          <img src="./img/mboutrecht.jpg" alt="logo" class="mbologo">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
          aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="inlog.php">Uitloggen</a>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  </div>


  <!-- De Jumbotron -->
  <div class="row">
    <div class="col-12">
      <div class="jumbotron jumbotron-fluid">
        <div class="container">

          <h1 class="display-4">MBO Utrecht - Update</h1>
          <p>

          </p>
        </div>
      </div>
    </div>
  </div>

  <!-- Opening van de Main-Container -->
  <main class="container">

    <!-- Het inschrijf formulier -->
    <div class="row">
      <div class="col-12">
        <form action="./update_script.php" method="post">
          <div class="form-group">
            <label for="firstname">Voornaam</label>
            <input type="text" class="form-control" id="firstname" aria-describedby="firstnameHelp" placeholder="Invoer voornaam"
              name="firstname" value="<?php echo $record["voornaam"] ?>">
          </div>
          <div class="form-group">
            <label for="infix">Tussenvoegsel</label>
            <input type="text" class="form-control" id="infix" aria-describedby="infixHelp" placeholder="Invoer tussenvoegsel"
              name="infix" value="<?php echo $record["infix"] ?>">
          </div>
          <div class="form-group">
            <label for="lastname">Achternaam</label>
            <input type="text" class="form-control" id="lastname" aria-describedby="lastnameHelp" placeholder="Invoer achternaam"
              name="lastname" value=" <?php echo $record["achternaam"] ?>">
            
          </div>
          <div class="form-group">
            <label for="email">Email</label>
            <input type="text" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Invoer email"
              name="email" value=" <?php echo $record["email"] ?>">
          </div>
          <div class="form-group">
            <label for="geboortedatum">Geboortedatum</label>
            <input type="date" class="form-control" id="geboortedatum" aria-describedby="geboortedatumHelp" placeholder="Invoer geboortedatum"
              name="geboortedatum" value=" <?php echo $record["geboortedatum"] ?>">
          </div>
          <div class="form-group">
            <label for="woonplaats">Woonplaats</label>
            <input type="text" class="form-control" id="woonplaats" aria-describedby="woonplaatsHelp" placeholder="Invoer woonplaats"
              name="woonplaats" value=" <?php echo $record["woonplaats"] ?>">
          </div>
          <div class="form-group">
            <label for="vooropleiding">Vooropleiding</label>
            <input type="text" class="form-control" id="vooropleiding" aria-describedby="vooropleidingHelp" placeholder="Invoer vooropleiding"
              name="vooropleiding" value=" <?php echo $record["vooropleiding"] ?>">
            <small id="emailHelp" class="form-text text-muted">Pas de betrokken gegevens aan en druk op opslaan.</small>
          </div>
          <input type="hidden" name="id" value=" <?php echo $id; ?> ">
          <button type="submit" class="btn btn-primary">Opslaan</button>
        </form>
      </div>
    </div>

  </main>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
    crossorigin="anonymous"></script>
  <script src=./js/app.js> </script> </body> </html> <style>
      <?php include './css/style.css'; ?>
    </style>