<?php 

    // Deze codes halen andere documenten erbij
    include("./connect_db.php");

    include("./functions.php");

    $firstname = sanitize($_POST["firstname"]);
    $infix = sanitize($_POST["infix"]);
    $lastname = sanitize($_POST["lastname"]);
    $email = sanitize($_POST["email"]);
    $geboortedatum = sanitize($_POST["geboortedatum"]);
    $woonplaats = sanitize($_POST["woonplaats"]);
    $vooropleiding = sanitize($_POST["vooropleiding"]);

    // Dit is in de taal sql een opdracht aan de database om een record toe te voegen aan de tabel */
    $sql = "INSERT INTO `student` (`id`, 
                                    `voornaam`, 
                                    `infix`, 
                                    `achternaam`, 
                                    `email`, 
                                    `geboortedatum`, 
                                    `woonplaats`, 
                                    `vooropleiding`) 
                                    VALUES (
                                    NULL, 
                                    '$firstname', 
                                    '$infix', 
                                    '$lastname', 
                                    '$email', 
                                    '$geboortedatum', 
                                    '$woonplaats', 
                                    '$vooropleiding');";

    // Maak en verstuur een sql-query via de verbinding $conn naar de mysql-server
    mysqli_query($conn, $sql);

    // Verlaat de create.php en ga naar index.php
    header("Location: ./inschrijven.php");

    

?>