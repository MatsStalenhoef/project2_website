<?php 
   
    include("./connect_db.php");

    $sql = "SELECT * FROM student";
    
    $result = mysqli_query($conn, $sql);

    $records = "";
    
    while ( $record = mysqli_fetch_assoc($result) ) 
    
    {

      $date = date_create($record["geboortedatum"]);
      
      if ($date != false) $date_nice = date_format($date,"d-m-Y");
      else $date_nice = '';
    
    $records .= "<tr>
                  <th scope='row'>" . $record["id"] . "</th>
                  <td>" . $record["voornaam"] . "</td>
                  <td>" . $record["infix"] . "</td>
                  <td>" . $record["achternaam"] . "</td>
                  <td>" . $record["email"] . "</td>
                  <td>" . $date_nice ."</td>
                  <td>" . $record["woonplaats"] . "</td>
                  <td>" . $record["vooropleiding"] . "</td>
                  <td>
                      <a href='./update.php?id=" . $record["id"] . "'>
                        <img src='./img/b_edit' alt='pencil' class='b_edit'>
                      <a>
                  </td>
                  <td>
                      <a href='./delete.php?id=" . $record["id"] . "'>
                        <img src='./img/b_drop' alt='cross' class='b_drop'>
                      <a>
                  </td>
                  </tr>";
                                                    

    }

?>
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    crossorigin="anonymous">
  <link rel="stylesheet" href="./css/style.css">

  <title>Adminpagina</title>
</head>

<body>

  <!-- De Nav-Bar -->
  <div class="row">
    <div class="col-12">
      <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="#">
          <img src="./img/mboutrecht.jpg" alt="logo" class="mbologo">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
          aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="inlog.php">Uitloggen<span class="sr-only">(current)</span></a>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  </div>


  <!-- De Jumbotron -->
  <div class="row">
    <div class="col-12">
      <div class="jumbotron jumbotron-fluid">
        <div class="container">

          <h1 class="display-4">MBO Utrecht - Adminpagina</h1>
          <p>

          </p>
        </div>
      </div>
    </div>
  </div>

  <!-- Opening van de Main-Container -->
  <main class="container">

    <!-- De Admintabel -->
    <div class="row">
      <div class="col-12">
        <table class="table table-hover">
          <thead>
            <tr>
              <th scope="col">Id</th>
              <th scope="col">Voornaam</th>
              <th scope="col">Tussenvoegsel</th>
              <th scope="col">Achternaam</th>
              <th scope="col">Email</th>
              <th scope="col">Geboortedatum</th>
              <th scope="col">Woonplaats</th>
              <th scope="col">Vooropleiding</th>
              <th scope="col">Edit</th>
              <th scope="col">Delete</th>
            </tr>
          </thead>
          <tbody>
            <?php
              echo $records;
            ?>
          </tbody>
        </table>
      </div>
    </div>

  </main>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
    crossorigin="anonymous"></script>
  <script src=./js/app.js> </script> </body> </html> <style>
      <?php include './css/style.css'; ?>
    </style>